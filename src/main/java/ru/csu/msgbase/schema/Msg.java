package ru.csu.msgbase.schema;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Msg {
    private Integer id;
    private String firstName;
    private String msg;
    private Timestamp time;
}
