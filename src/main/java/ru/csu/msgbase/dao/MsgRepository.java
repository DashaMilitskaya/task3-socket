package ru.csu.msgbase.dao;

import ru.csu.msgbase.schema.Msg;

import java.util.LinkedList;
import java.util.List;

public interface MsgRepository {
    LinkedList<Msg> getHistory();

    void save(Msg user);
}
