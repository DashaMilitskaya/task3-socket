package ru.csu.msgbase.dao;

import ru.csu.msgbase.schema.Msg;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;

public class MsgRepositoryImpl {
    static String url = "jdbc:mariadb://localhost:3306/java";
    static String username = "root";
    static String password = "";

    static {

        try {
            Class.forName("org.mariadb.jdbc.Driver").getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException |
                 ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static LinkedList<Msg> getHistory() {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement("SELECT * FROM Msgs  ORDER BY time DESC LIMIT 10");
            var result = statement.executeQuery();

            LinkedList<Msg> list = new LinkedList<Msg>();
            while (result.next()) {
                list.add(Msg
                        .builder()
                        .id(result.getInt("id"))
                        .firstName(result.getString("first_name"))
                        .msg(result.getString("msg"))
                        .time(result.getTimestamp("time"))
                        .build());
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static void save(Msg message) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn
                    .prepareStatement("INSERT INTO  msgs (first_name, msg, time) VALUES (?, ?, ?)");
            statement.setString(1, message.getFirstName());
            statement.setString(2, message.getMsg());
            statement.setTimestamp(3, message.getTime());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return;
    }


}
